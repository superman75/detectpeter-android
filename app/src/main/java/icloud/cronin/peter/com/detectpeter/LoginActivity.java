package icloud.cronin.peter.com.detectpeter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;



public class LoginActivity extends AppCompatActivity {
    static final int GALLERY_REQUEST = 123;
    public static final String PREFS_NAME = "MyPrefsFile";
    EditText sEmail,sPassword,rEmail;
    Uri currImageURI;
    String currImageURIString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        sEmail = findViewById(R.id.sender_email);
        sPassword = findViewById(R.id.sender_password);
        rEmail = findViewById(R.id.receiver_email);
        currImageURIString="android.resource://" + getPackageName() + "/" + R.raw.captain;


    }


    public void login(View view) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("sEmail",sEmail.getText().toString());
        editor.putString("sPassword",sPassword.getText().toString());
        editor.putString("rEmail",rEmail.getText().toString());
        editor.putBoolean("Flag",false);
        editor.putString("videoUri",currImageURIString);
        // Commit the edits!
        editor.apply();

        Intent i = new Intent(this, VideoActivity.class);
        startActivity(i);
        finish();
    }

    public void chooseVideo(View view) {
// To open up a gallery browser
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"),GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode == GALLERY_REQUEST){
                currImageURI = data.getData();
                currImageURIString = getRealPathFromURI(currImageURI);
            }
        }
    }
    // And to convert the image URI to the direct file system path of the image file
    public String getRealPathFromURI(Uri contentUri) {

        // can post image
        String [] proj={MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery( contentUri,
                proj, // Which columns to return
                null,       // WHERE clause; which rows to return (all rows)
                null,       // WHERE clause selection arguments (none)
                null); // Order-by clause (ascending by name)
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }
}
