package icloud.cronin.peter.com.detectpeter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ProgressBar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
/**
 * Created by peter on 6/28/2017.
 */
public class MotionDetector {
    class MotionDetectorThread extends Thread{
        private AtomicBoolean isRunning = new AtomicBoolean(true);

        public void stopDetection(){
            isRunning.set(false);
        }

        @Override
        public void run() {
            while (isRunning.get()) {

                long now = System.currentTimeMillis();
                if (now-lastCheck > checkInterval) {
                    Log.e("MotionDetector : ", "diff : "+(now-lastCheck));
                    lastCheck = now;

                    if (nextData.get() != null) {
                        int[] img = ImageProcessing.decodeYUV420SPtoLuma(nextData.get(), nextWidth.get(), nextHeight.get());
//                        int[] img = ImageProcessing.decodeYUV420SPtoRGB(nextData.get(), nextWidth.get(), nextHeight.get());


                        int lumaSum = 0;
                        for (int i : img) {
                            lumaSum += i;
                        }
                        if (lumaSum < minLuma) {
                            if (motionDetectorCallback != null) {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        motionDetectorCallback.onTooDark();
                                    }
                                });
                            }
                        } else if (detector.detect(img, nextWidth.get(), nextHeight.get())) {
                            if (motionDetectorCallback != null) {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        motionDetectorCallback.onMotionDetected();
                                    }
                                });
                            }
//                            Bitmap bitmap = ImageProcessing.lumaToGreyscale(img,nextWidth.get(), nextHeight.get());
//                            Bitmap bitmap = ImageProcessing.rgbToBitmap(img,nextWidth.get(), nextHeight.get());
//                            bitmap = ImageProcessing.rotate(bitmap,-90);

                            File photo = new File(Environment.getExternalStorageDirectory()+"/DCIM/Camera",  "sample.jpg");
                            if (photo.exists()) photo.delete();

                            try {
                                FileOutputStream fos = new FileOutputStream(photo.getPath());
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                                Log.e("MotionDetector : ", " bitmap=" + bitmap);
                                fos.close();
                            } catch (java.io.IOException e) {
                                Log.e("PictureDemo", "Exception in photoCallback", e);
                            }

                        }
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final AggregateLumaMotionDetection detector;
    private long checkInterval = 500;
    private long lastCheck = 0;
    private MotionDetectorCallback motionDetectorCallback;
    private Handler mHandler = new Handler();

    private AtomicReference<byte[]> nextData = new AtomicReference<>();
    private AtomicInteger nextWidth = new AtomicInteger();
    private AtomicInteger nextHeight = new AtomicInteger();
    private int minLuma = 1000;
    private MotionDetectorThread worker;

    public Camera mCamera;
    private boolean inPreview;
    private SurfaceHolder previewHolder;
    private Context mContext;
    private SurfaceView mSurface;
    private Bitmap bitmap;

    public MotionDetector(Context context, SurfaceView previewSurface){
        detector = new AggregateLumaMotionDetection();
        mContext = context;
        mSurface = previewSurface;
    }

    public void setMotionDetectorCallback(MotionDetectorCallback motionDetectorCallback){
        this.motionDetectorCallback =  motionDetectorCallback;

    }

    public void consume(byte[] data, int width, int height){
        nextData.set(data);
        nextWidth.set(width);
        nextHeight.set(height);
    }

    public void setCheckInterval(long checkInterval) {
        this.checkInterval = checkInterval;
    }

    public void setMinLuma(int minLuma) {
        this.minLuma = minLuma;
    }

    public void setLeniency(int l) {
        detector.setLeniency(l);
    }

    public void onResume(){
        if (checkCameraHardware()){
            mCamera = getCameraInstance();

            worker = new MotionDetectorThread();
            worker.start();

            previewHolder = mSurface.getHolder();
            previewHolder.addCallback(surfaceCallback);
            previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        }
    }

    public boolean checkCameraHardware(){
        if (mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){

            return true;
        }else{
            return false;

        }
    }

    private Camera getCameraInstance(){
        Camera c = null;

         try {
             if (Camera.getNumberOfCameras() >= 2) {
                 c = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);

             } else {
                 c = Camera.open();
             }
         }
         catch (Exception e){

        }

        return c;
    }


    private Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {

        @Override
        public void onPreviewFrame(byte[] data, Camera cam) {
            if (data == null) return;
            Camera.Size size = cam.getParameters().getPreviewSize();
            if (size == null) return;

            Camera.Parameters parameters = cam.getParameters();
            int width = parameters.getPreviewSize().width;
            int height = parameters.getPreviewSize().height;

            YuvImage yuv = new YuvImage(data, parameters.getPreviewFormat(), width, height, null);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            yuv.compressToJpeg(new Rect(0, 0, width, height), 50, out);

            byte[] bytes = out.toByteArray();
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            Matrix matrix = new Matrix();
            matrix.postRotate(-90);
            matrix.postScale(-1, 1, width/2, height/2);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);

            consume(data, size.width, size.height);
        }
    };

    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback(){

        @Override
        public void surfaceCreated(SurfaceHolder holder){
            try{
                mCamera.setPreviewDisplay(previewHolder);
                mCamera.setPreviewCallback(previewCallback);

            }catch (Throwable t){
                Log.e("MotionDetector", "Exception in setPreviewDisplay", t);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){
            Camera.Parameters parameters = mCamera.getParameters();
            Camera.Size size = getBestPreviewSize(width, height, parameters);
            if (size != null){
                parameters.setPictureSize(size.width, size.height);
                Log.d("MotionDetector", "Using width=" + size.width + "height=" + size.height);
            }
            mCamera.setParameters(parameters);
            mCamera.startPreview();
            inPreview = true;
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder){

        }
    };

    private static Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters){
     Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPictureSizes()){
            if (size.width <= width && size.height <= height){
            if (result == null){
                result = size;
            }else {
                int resultArea = result.width * result.height;
                int newArea = size.width * size.height;

                if (newArea > resultArea) result = size;
            }
            }
        }

        return result;

    }
     public void onPause() {
         releaseCamera();
         if (previewHolder != null) previewHolder.removeCallback(surfaceCallback);
         if (worker != null) worker.stopDetection();
     }

     private void releaseCamera(){
         if (mCamera != null){
             mCamera.setPreviewCallback(null);
             if (inPreview) mCamera.stopPreview();
             inPreview = false;
             mCamera.release();
             mCamera = null;
         }
     }

}

