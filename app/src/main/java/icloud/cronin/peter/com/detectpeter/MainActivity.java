package icloud.cronin.peter.com.detectpeter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class MainActivity extends AppCompatActivity{

    public static final String PREFS_NAME = "MyPrefsFile";
//    private TextView txtStatus;
    private MotionDetector motionDetector;


    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);




        motionDetector = new MotionDetector(this, (SurfaceView) findViewById(R.id.surfaceView));
//        txtStatus = (TextView) findViewById(R.id.txtStatus);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String currImageUriString = settings.getString("videoUri",null);
        SharedPreferences.Editor editor = settings.edit();
//        username = settings.getString("sEmail","");
//        password = settings.getString("sPassword","");
//        receiver = settings.getString("rEmail","");
        editor.putBoolean("Flag",true);
        editor.apply();

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        Log.d("FilePath",currImageUriString);
        retriever.setDataSource(getApplication(),Uri.parse(currImageUriString));

        Bitmap bitmap = retriever.getFrameAtTime(1,MediaMetadataRetriever.OPTION_CLOSEST);
        imageView = findViewById(R.id.imageView);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        params.width = width;
        params.height = height;
        imageView.setImageBitmap(bitmap);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        motionDetector.setMotionDetectorCallback(new MotionDetectorCallback(){

            @Override
            public void onMotionDetected(){
//                sendEmail();
                motionDetector.onPause();
                startActivity(new Intent(getApplicationContext(),DetectDetect.class));

                finish();

//
            }

           @Override
            public void onTooDark() {
//                txtStatus.setText("To dark here");
            }

        });
    }

    @Override
    protected void onResume(){
        super.onResume();
        motionDetector.onResume();


    }

    @Override
    protected void onPause(){
        super.onPause();
        motionDetector.onPause();
    }



}
