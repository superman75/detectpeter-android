package icloud.cronin.peter.com.detectpeter;

/**
 * Created by peter on 6/30/2017.
 */

public interface MotionDetectorCallback {
    void onMotionDetected();
    void onTooDark();

}
