package icloud.cronin.peter.com.detectpeter;

/**
 * Created by peter on 6/29/2017.
 */

public class AggregateLumaMotionDetection implements IMotionDetection {


    private int mLeniency = 20;

    private static final int mDebugMode = 2;
    private static final int mXBoxes = 10;
    private static final int mYBoxes = 10;

    private static int[] mPrevious = null;
    private static int mPreviousWidth;
    private static int mPreviousHeight;
    private static State mPreviousState = null;




    @Override
    public int[] getPrevious(){
        return ((mPrevious != null) ? mPrevious.clone() : null);
    }

    protected boolean isDifferent(int[] first, int width, int height){
        if (first == null) throw new NullPointerException();

        if (mPrevious == null) return false;
        if (first.length != mPrevious.length)return true;
        if (mPreviousWidth != width || mPreviousHeight != height) return true;

        if (mPreviousState == null){
            mPreviousState = new State(mPrevious, mPreviousWidth, mPreviousHeight);
            return false;
        }

        State state = new State(first, width, height);
        Comparer comparer = new Comparer(state, mPreviousState, mXBoxes, mYBoxes, mLeniency, mDebugMode);

        boolean different = comparer.isDifferent();


        mPreviousState = state;

        return different;
    }




    @Override
    public boolean detect(int[] luma, int width, int height){
        if (luma == null) throw new NullPointerException();

        int[] original = luma.clone();

        if (mPrevious == null){
            mPrevious = original;
            mPreviousWidth = width;
            mPreviousHeight = height;
        }



        boolean motionDetected = isDifferent(luma, width, height);

        mPrevious = original;
        mPreviousWidth = width;
        mPreviousHeight = height;

        return motionDetected;
    }

    public void setLeniency(int l){
        mLeniency = l;
    }


}

