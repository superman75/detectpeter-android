package icloud.cronin.peter.com.detectpeter;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class EmailIntent extends IntentService {

    public static final String TAG = "EmailIntentDemo";

    private String uname = "";
    private String pass = "";
    private String rname = "";
    public EmailIntent() {
        super("EmailIntent");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e(TAG, "Service Commenced!");
        final String username;
        final String password;
        final String receiver;

        username = intent.getStringExtra("senderEmail");
        password = intent.getStringExtra("password");
        receiver = intent.getStringExtra("recieverEmail");


        String root = Environment.getExternalStorageDirectory().toString();
        String filename = root + "/DCIM/Camera/sample.jpg";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication(
                                username, password);
                    }
                });
        // TODO Auto-generated method stub
        Message message = new MimeMessage(session);
////        Log.e(username,password);
        try {
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(receiver));
            message.setSubject("New Message From DetectPeter");

        } catch (MessagingException e) {
            e.printStackTrace();
        }


        if (!"".equals(filename)) {
            Multipart _multipart = new MimeMultipart();
            BodyPart messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(filename);

            try {
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);

                _multipart.addBodyPart(messageBodyPart);
                message.setContent(_multipart);
            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }
        try {
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }


        Log.e("sending...","from "+username+" to "+receiver);
    }

}
