package icloud.cronin.peter.com.detectpeter;

/**
 * Created by peter on 6/30/2017.
 */

public interface IMotionDetection {
    public int[] getPrevious();

    public boolean detect(int[] data, int width, int height);
}
