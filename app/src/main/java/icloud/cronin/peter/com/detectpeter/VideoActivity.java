package icloud.cronin.peter.com.detectpeter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.VideoView;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class VideoActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "MyPrefsFile";
    private VodView mvideo;
    private ScaleGestureDetector mScaleGestureDetector;
    private FrameLayout.LayoutParams mRootParam;
    static final int MIN_WIDTH = 100;
    SharedPreferences settings;
    boolean first_flag = false;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video);

        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String currImageUriString = settings.getString("videoUri",null);

        mvideo = findViewById(R.id.video);
        mRootParam = (FrameLayout.LayoutParams) (findViewById(R.id.root_view)).getLayoutParams();
        Uri uri = Uri.parse(currImageUriString);
        mvideo.setVideoURI(uri);



        first_flag = settings.getBoolean("Flag",false);
        if (first_flag) {
//            sendEmail();
            Intent intent = new Intent(this, EmailIntent.class);
            String sender = settings.getString("sEmail","");
            String password = settings.getString("sPassword","");
            String reciever = settings.getString("rEmail","");
            intent.putExtra("senderEmail",sender);
            intent.putExtra("password",password);
            intent.putExtra("recieverEmail",reciever);
            startService(intent);

        }



        mvideo.start();

        mvideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                startDetection();
               finish();
            }
        });



        mScaleGestureDetector = new ScaleGestureDetector(this, new MyScaleGestureListener());
        mvideo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
  //            mvideo.start();
                mScaleGestureDetector.onTouchEvent(event);
                return true;
            }

        });
    }

    @Override
    protected void onResume() {
     //   Boolean video_start = settings.getBoolean("Flag",false);
      //  if (video_start) mvideo.start();
        super.onResume();

    }





    private void startDetection(){
        startActivity(new Intent(this,MainActivity.class));

        finish();
    }


    private class MyScaleGestureListener implements ScaleGestureDetector.OnScaleGestureListener {
        private int mW, mH;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            mW *= detector.getScaleFactor();
            mH *= detector.getScaleFactor();
            if (mW < MIN_WIDTH) {
                mW = mvideo.getWidth();
                mH = mvideo.getHeight();

            }

//            Log.d("onScale", "scale=" + detector.getScaleFactor() + ", w=" + mW + ", h=" + mH);
            mvideo.setFixedVideoSize(mW, mH);
            mRootParam.width = mW;
            mRootParam.height = mH;
            return true;

        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            mW = mvideo.getWidth();
            mH = mvideo.getHeight();
            Log.d("onScaleBegin", "scale=" + detector.getScaleFactor() + ", w=" + mW + ", h=" + mH);
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            Log.d("onScaleEnd", "scale=" + detector.getScaleFactor() + ", w=" + mW + ", h=" + mH);
        }
    }
}
